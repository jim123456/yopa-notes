<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdditionalNote extends Model
{
    protected $fillable = [
        'body',
        'user_id',
        'note_id'
    ];
    
    protected $rules = [
        'body'  =>  'required | string | max:1000',
        'user_id'   =>  'required | integer',
        'note_id'   =>  'required | integer'
    ];
    
    public function getCreateRules () {
        return $this->rules;
    }
    
    public function getEditRules () {
        return $this->rules;
    }
    
    public function author () {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
    
    public function note () {
        return $this->belongsTo(Note::class, 'note_id', 'id');
    }
}
