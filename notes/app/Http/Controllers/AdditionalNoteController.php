<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Note;
use App\AdditionalNote;
use Validator;

class AdditionalNoteController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    
    public function create (Note $note) {
        
        $user = Auth::user();
        
        $notes = $note->additionalNotes;
        $notes->push($note);
        
        return view('pages.additionalNotes.create',[
            
            'user'  =>   $user,
            'note'  =>  $note,
            'notes' =>  $notes
            
            ]);
        
    }
    
    public function store (Request $request) {
        
        $additionalNote = new AdditionalNote();
        
        $additionalNote->fill($request->toArray());
        $additionalNote->note_id = $request->note_id;
        
        $validator = Validator::make($additionalNote->toArray(), $additionalNote->getCreateRules());
        
        if ($validator->fails()) {
            
            return redirect()->back()->withInput()->withErrors($validator);
        }
        
        try { 
            
            $additionalNote->save();
            
        } catch (\Exception $e) {
            return redirect()->back()->withInput()->withErrors($e);
        }
        
        $request->session()->flash('success', 'Additional note successfully created');
        return redirect('/notes/' . $additionalNote->note->id);
        
    }
}
