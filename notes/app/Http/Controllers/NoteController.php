<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Note;
use Auth;
use Validator;
use Carbon\Carbon;

class NoteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index () 
    {
        $notes = Note::all();
        $notes = $notes->sortByDesc('created_at');
        
        return view('pages.notes.index', [
            
            'notes' =>  $notes,

            ]);
    }
    
    public function create () {
        
        $user=Auth::user();
        
        return view('pages.notes.create', [
            
            'user'  =>  $user,
            
            ]);
        
    }
    
    public function store (Request $request) {
        
        $note = new Note();
        
        $note->fill($request->toArray());
        
        $validator = Validator::make($note->toArray(), $note->getCreateRules());
      
        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        
        try { 
            
            $note->save();
            
        } catch (\Exception $e) {
            return redirect()->back()->withInput()->withErrors($e);
        }
        
        $request->session()->flash('success', 'Note successfully created');
        return redirect('/notes/'. $note->id);
        
    }
    
    public function show (Note $note) {
        
        $notes = $note->additionalNotes;
        $notes = $notes->sortByDesc('created_at');
        $notes->push($note);

        return view('pages.notes.show', [
            
            'note' => $note,
            'notes' =>  $notes
            
            ]);
        
    }
}
