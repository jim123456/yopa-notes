<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    protected $fillable = [
        'title',
        'body',
        'user_id'
    ];
    
    protected $rules = [
        'title' =>  'required | string | max:50',
        'body'  =>  'required | string | max:1000',
        'user_id'   =>  'required | integer'
    ];
    
    public function getCreateRules () {
        return $this->rules;
    }
    
    public function getEditRules () {
        return $this->rules;
    }
    
    public function author () {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
    
    public function additionalNotes () {
        return $this->hasMany(AdditionalNote::class, 'note_id', 'id');
    }
    
}
