@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class='col-md-12'>
            <h3>Login</h3>
            
            <form method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
                
                <div class='row'>
                    <div class='col-md-6'>
                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }} required">
                            <label for='title' class='control-label'>Username: </label>
                            <input type='text' name='username' id='username' class='form-control' value="{{ old('username') }}" required>
                             @if ($errors->has('username'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('username') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                
                <div class='row'>
                    <div class='col-md-6'>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} required">
                            <label for='password' class='control-label'>Password: </label>
                            <input type='password' name='password' id='password' class='form-control' required>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class='row'>
                    <div class='col-md-6'>
                        <button type="submit" class="btn btn-success btn-square">
                            LOGIN &nbsp >
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
