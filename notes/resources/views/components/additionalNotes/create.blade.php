<div class='container-fluid'>

    <div class='row'><!--Header row-->
        <div class='col-md-8 col-sm-12 col-xs-12'>
            <h3>{{$note->title}}</h3>   
            @include('components.messages.session_success_message') 
            @include('components.messages.session_validation_message')
        </div>
    </div>

    <form action='/additional-notes/store' method='post'>
            
    {{ csrf_field() }}
        
    <input type='hidden' name='user_id' id='user_id' value='{{$user->id}}'>
    <input type='hidden' name='note_id' id='note_id' value='{{$note->id}}'>
            
    <div class='row'>
        <div class='col-md-8'>
            <form-group>
                <label for='body' class='sr-only'>Text</label>
                <textarea name='body' id='body' class='form-control' rows='5' value="{{ old('body') }}" ></textarea>
            </form-group>
        </div>
    </div>
            
    <br>
            
    <div class='row form-group'>
        <div class='col-md-8 col-sm-12 col-xs-12'>
            <div class='row'>
                <div class='col-md-6 col-sm-6 col-xs-5'>
                    <a href="/notes/{{$note->id}}" class="btn btn-default btn-transparent btn-square" role="button">Cancel</a>
                </div>  
                <div class='col-md-6 col-sm-6 col-xs-5 text-right'>    
                    <button type="submit" class="btn btn-success btn-square">ADD NOTE &nbsp ></button>
                </div>
            </div>
        </div>
    </div>            

    </form>
    
    <br>
    
    @foreach($notes as $note)
    <div class='row form-group'><!--Row 1-->
        <div class='col-md-8 col-sm-12 col-xs-12'>
            
            <div class='row'><!--Row containing image and speech bubble-->
                <div class='col-md-2'>
                    <img src="{{$note->author->image_url}}" alt="{{$note->author->username}}" class="img-circle img-responsive">
                </div>
                
                <div class='col-md-10'>
                    <div class='triangle-border left'><!--Speech bubble-->
                        
                        <div class='row form-group'><!--Username and created at row-->
                            <div class='col-md-6'><strong>{{$note->author->username}}</strong></div>
                            <div class='col-md-6 text-right'>
                                 <strong>
                                    @if($note->created_at->isToday())
                                        Today at {{$note->created_at->format('h:i')}}
                                    @elseif ($note->created_at->isYesterday())
                                        Yesterday at {{$note->created_at->format('h:i')}}
                                    @else
                                        {{$note->created_at->format('d/m/y')}} at {{$note->created_at->format('h:i')}}
                                    @endif
                                </strong>
                            </div>
                        </div>
                        
                        <div class='row form-group'><!--Note text row-->
                            <div class='col-md-12'>
                                {{$note->body}}                              
                            </div>
                        </div>
                        
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    @endforeach
    </div>
</div>