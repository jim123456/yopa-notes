@if(session('app_error'))
      <div class="alert alert-danger alert-dismissable">
            {{ session('app_error') }}
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
        </div>
@endif