@if (session('success'))
    <div class="alert alert-success alert-dismissable">
        {{ session('success') }}
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    </div>
@endif