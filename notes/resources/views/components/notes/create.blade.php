<div class='container-fluid'>
    <div class='col-md-8 col-sm-12 col-xs-12'>
        <h3>New note</h3>
     
         <form action='/notes/store' method='post'>
            {{ csrf_field() }}
            <input type='hidden' name='user_id' id='user_id' value='{{$user->id}}'>

             @include('components.messages.session_validation_message')

            <div class='form-group'>
                <label for='title'>Title</label>
                <input type='text' name='title' id='title' class='form-control' required='true'>                
            </div>
            
             <div class='form-group'>
                <label for='body'>Note</label>
                <textarea name='body' id='body' class='form-control' rows='5'></textarea>                
            </div>
            
            <div class='row'>
            
                <div class='col-md-6'>    
                    <a href="/notes" class="btn btn-default btn-transparent btn-square"  role="button">Cancel</a>
                </div>
            
                <div class='col-md-6 text-right'>    
                    <button type="submit" class="btn btn-success btn-square">ADD NOTE &nbsp ></button>
                </div>
                
            </div>
            
        </form>

    </div><!--End of col-->
   
</div>