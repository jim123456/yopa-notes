
<div class='container-fluid'>

    <div class='col-md-8 col-sm-12 col-xs-12'>
        <div class='row'>
            <div class='col-md-6 col-sm-6 col-xs-6'>
                <h3>Notes overview</h3>    
            </div>
            
            <div class='col-md-6 col-sm-6 col-xs-6 text-right'>
                <br>
                <p>
                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        Logout
                    </a>
                </p>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </div>
        </div><!--End of row 1-->
    
        <div class='panel panel-default'><!--Start of panel-->
            <div class='panel-body'>
                <div class='table-responsive'><!--Start of row 2-->
                    <table class='table js-table custom-table-bottom'>
                        <tr class='row-custom'>
                            <th style="width: 20%;">Created by</th>
                            <th style="width: 60%;">Title</th>     
                            <th style="width: 20%;">Date</th>     
                        </tr>
                    
                    @if($notes->count() > 0)
                        @foreach($notes as $note)
                        <tr class='table-tr' data-url='/notes/{{$note->id}}'>
                            <td>{{$note->author->username}}</td>
                            <td>{{$note->title}}</td>
                            @if($note->created_at->isToday())
                            <td>Today at {{$note->created_at->format('h:i')}}</td>
                            @elseif ($note->created_at->isYesterday())
                            <td>Yesterday at {{$note->created_at->format('h:i')}}</td>
                            @else
                            <td>{{$note->created_at->format('d/m/y')}} at {{$note->created_at->format('h:i')}}</td>
                            @endif
                        </tr>
                        @endforeach
                    </table>    
                    @else
                    </table>  
                    
                    <h2>There are currently no notes</h2>
                    @endif
                </div><!--End of responsive table div-->
                
                <a href="/notes/create" class="btn btn-success btn-square" role="button">CREATE A NEW NOTE &nbsp ></a>
            
            </div><!--End of panel body-->
            
        </div><!--End of panel-->
    </div>
</div>