<div class='container-fluid'>

    <div class='row'><!--Header row-->
        <div class='col-md-8 col-sm-12 col-xs-12'>
            <h3>{{$note->title}}</h3>   
            @include('components.messages.session_success_message') 
        </div>
    </div>

    <br>

    <div class='row form-group'><!--Buttons row-->
        <div class='col-md-8 col-sm-12 col-xs-12'>
            <div class='row'>
                <div class='col-md-6 col-sm-6 col-xs-5'>    
                    <a href="/notes" class="btn btn-default btn-transparent btn-square" role="button">< &nbsp Back to overview</a>
                </div>
                <div class='col-md-6 col-sm-6 col-xs-5 text-right'>    
                    <a class="btn btn-success btn-square" href="/additional-notes/{{$note->id}}/create" role="button">ADD ADDITIONAL NOTE &nbsp ></a>
                </div>
            </div>
        </div>
    </div>
    
    <br>
    
    @foreach($notes as $note)
    <div class='row form-group'><!--Row 1-->
        <div class='col-md-8 col-sm-12 col-xs-12'>
            
            <div class='row'><!--Row containing image and speech bubble-->
                <div class='col-md-2'>
                    <img src="{{$note->author->image_url}}" alt="{{$note->author->username}}" class="img-circle img-responsive">
                </div>
                
                <div class='col-md-10'>
                    <div class='triangle-border left'><!--Speech bubble-->
                        
                        <div class='row form-group'><!--Username and created at row-->
                            <div class='col-md-6'><strong>{{$note->author->username}}</strong></div>
                            <div class='col-md-6 text-right'>
                                <strong>
                                    @if($note->created_at->isToday())
                                        Today at {{$note->created_at->format('h:i')}}
                                    @elseif ($note->created_at->isYesterday())
                                        Yesterday at {{$note->created_at->format('h:i')}}
                                    @else
                                        {{$note->created_at->format('d/m/y')}} at {{$note->created_at->format('h:i')}}
                                    @endif
                                </strong>
                            </div>
                        </div>
                        
                        <div class='row form-group'><!--Note text row-->
                            <div class='col-md-12'>
                                {{$note->body}}                              
                            </div>
                        </div>
                        
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    @endforeach
    </div>
</div>