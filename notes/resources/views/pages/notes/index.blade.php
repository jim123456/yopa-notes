@extends('layouts.app')

@section('content')

    @include('components.notes.index')

@endsection

@section('additionalJSFooter')
<script type='text/javascript'>
$(function () {

    $(".js-table").on("click", "tr[data-url]", function () {
        window.location = $(this).data("url");
    });

});
</script>
@endsection