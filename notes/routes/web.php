<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'NoteController@index');

Auth::routes();

//Notes
Route::get('/home', 'NoteController@index');
Route::get('/notes', 'NoteController@index');
Route::get('/notes/create', 'NoteController@create');
Route::post('/notes/store', 'NoteController@store');
Route::get('/notes/{note}', 'NoteController@show');

//Additional notes
Route::get('/additional-notes/{note}/create', 'AdditionalNoteController@create');
Route::post('/additional-notes/store', 'AdditionalNoteController@store');

